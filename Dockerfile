FROM docker.io/maptiler/tileserver-gl-light:latest

USER 0

RUN apt-get update && apt-get install -y wget

ADD --chmod=755 https://dl.min.io/client/mc/release/linux-amd64/mc \
    /usr/local/bin/mc

ADD --chmod=777 https://github.com/maptiler/tileserver-gl/releases/download/v1.3.0/zurich_switzerland.mbtiles \
    /opt/sample.mbtiles

USER 1000
CMD ["--mbtiles", "/opt/sample.mbtiles"]
